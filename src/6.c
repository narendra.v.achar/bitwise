unsigned int left_rotate_bits(unsigned int num)
{
	return (num << 1) | (num >> (sizeof(int) * 8 - 1));
}

unsigned int right_rotate_bits(unsigned int num)
{
	return (num >> 1) | (num << (sizeof(int) * 8 - 1));
}

unsigned int left_rotate_n_bits(unsigned int num, int n)
{
	return (num << n) | (num >> (sizeof(int) * 8 - n));
}

unsigned int right_rotate_n_bits(unsigned int num, int n)
{
	return (num >> n) | (num << (sizeof(int) * 8 - n));
}
