unsigned int copy_bits (unsigned int snum, unsigned int dnum, unsigned int n, unsigned int s, unsigned int d)
{
	unsigned int op1 = snum & ((~(~0 << n)) << (s - n + 1 ));
	unsigned int op3 =(op1 >> (s - n + 1 )) << (d - n + 1);
	unsigned int op2 = dnum & ~(((1U << n)-1) << (d - n + 1));
	             
	dnum = op3 | op2;
	
	return dnum;
}
