unsigned int even_bit_toggle(unsigned int num)
{
	for (int i = (sizeof(int) * 8) - 1; i >= 0; i--) {
		if((i % 2) == 0)
			num = num ^ (1 << i);
	}

	return num;
}


unsigned int odd_bit_toggle(unsigned int num)
{
	for (int i = (sizeof(int) * 8) - 1; i >= 0; i--) {
		if((i % 2) == 1)
			num = num ^ (1 << i);
	}

	return num;
}
