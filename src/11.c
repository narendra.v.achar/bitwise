unsigned int invert(unsigned int x, unsigned int p, unsigned int n)
{
	unsigned int temp = (~(~0 << n)) << (p - n + 1);

	return x ^ temp;
}
