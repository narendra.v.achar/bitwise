#ifndef HEADER_H
#define HEADER_H

/* 5. test and set a bit */

#define bit_ts(num, pos) (num | (1 << pos))

/* 9. */

#define maxmin(a, b) (a - ((((a - b) >> 31) & 1) * (a - b)))

#define clear_right_most_bit(num) (num & ~1)

#define clear_left_most_bit(num) (num & ~(1 << (sizeof(int) * 8 - 1)))

#define set_right_most_bit(num) (num | 1)

#define set_left_most_bit(num) (num | (1 << (sizeof(int) * 8 - 1)))

#define get_bits(x, p, n)  (x >> (p)) & (~(~0 << n))

#define SET_BIT_FROM_S_TO_D(num,...){ \
	while(s!=(d-1))	\
	{		\
	 store = num | (1<<s);\
         add += store; \
                s--;	\
        }		\
        printf("Value after setbit: %d\n",add);\
	}

#define CLEAR_BIT_FROM_S_TO_D(num,...){ \
	while(s!=(d-1))	\
	{		\
	 num &= ~(1<<s);\
                s--;	\
        }		\
        printf("Value after clear bit: %d\n",num);\
	}

#define TOGGLE_BIT_FROM_S_TO_D(num,...){ \
	while(s!=(d-1))	\
	{		\
	 num ^= (1<<s);\
                s--;	\
        }		\
        printf("Value after toggle bit: %d\n",num);\
	}

/* Function declarations  */

unsigned int swap_bits(unsigned int num, unsigned int s, unsigned int d);

void swap_bits_between(unsigned int *snum, unsigned int *dnum, unsigned int s, unsigned int d);

unsigned int copy_bits (unsigned int snum, unsigned int dnum, unsigned int n, unsigned int s, unsigned int d);

unsigned int odd_bit_toggle(unsigned int num);

unsigned int even_bit_toggle(unsigned int num);

unsigned int right_rotate_n_bits(unsigned int num, int n);

unsigned int left_rotate_n_bits(unsigned int num, int n);

unsigned int right_rotate_bits(unsigned int num);

unsigned int left_rotate_bits(unsigned int num);

int count_bit_clear(int num);

int count_bit_set(int num);

int cnt_trailing_clear_bits(unsigned int num);

int cnt_trailing_set_bits(unsigned int num);

int cnt_leading_clear_bits(unsigned int num);

int cnt_leading_set_bits(unsigned int num);

unsigned int setbits(unsigned int x, unsigned int p, unsigned int n, unsigned int y);

unsigned int invert(unsigned int x, unsigned int p, unsigned int n);

#endif
